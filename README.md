## A project contains 2 Dyanamic Web Projects 
  1. hello.war 
  2.HelloWorld.war
  
## Run And Deploy into JBoss EAP7 Steps

1. This project contians a build.xml need to run it using ant CLI commands  in the local or any server 
2. The below entries to ba modifed according to your JBoss Server intallation Path

    i) <property name="jboss" value="C:/jboss/jboss-eap-7.0" />  At lin Number : 3 
   
3. The Ant script will take the responsebility of deploying into JBoss server

## To test the end points are 

1. To test the hello.war project enpoint is :  http://localhost:4040/hello/sample.jsp
2. To test the HelloWorld.war project endpoint is : http://localhost:4040/HelloWorld/login.jsp

NOTE : Sever host and port may warry according to your JBoss EAP 7 server

